const { DataTypes } = require("sequelize");

// определяем модель Lesson
module.exports = (sequalize) => {
  return sequalize.define("lessons", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING
    },
    topic: {
      type: DataTypes.STRING
    },
    group: {
      type: DataTypes.STRING
    },
    teacher: {
      type: DataTypes.STRING
    },
    room: {
      type: DataTypes.INTEGER
    },
    time: {
      type: DataTypes.STRING
    }
  });
}