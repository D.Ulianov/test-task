const { DataTypes } = require("sequelize");
 
// определяем модель Students
module.exports = (sequelize) => {
  return sequelize.define("students", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    },
    age: {
      type: DataTypes.INTEGER
    }
  });
}