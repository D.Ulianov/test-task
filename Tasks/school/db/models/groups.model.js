const { DataTypes } = require("sequelize");
 
// определяем модель Groups
module.exports = (sequelize) => {
  return sequelize.define("groups", {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING
    },
    members: {
      type: DataTypes.INTEGER
    }
  });
}