const { models } = require('../models')

function applySetupModel(sequelize) {
	const { lessons, groups, students } = sequelize.models;

	lessons.hasMany(groups);
	groups.hasMany(students, { onDelete: 'cascade' });
}

module.exports = { applySetupModel };