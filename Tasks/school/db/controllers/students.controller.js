const { models } = require('..');
const groupControll = require('./groups.controller')

const findAll = async () => {
  return await models.students.findAll()
  .then(async (data) => {
    if(data) return { students: await assemberData(data.dataValues) };
    return {students : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const findOne = async (id) => {
  return await models.students.findOne({where: {id}})
  .then(async (data) => {
    if(data) return { students: await assemberData(data.dataValues) };
    return {students : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const create = async (new_st) => {
 return await models.students.create(new_st)
  .then( async(data) => { return { students: await assemberData(data.dataValues)}})
  .catch(err => console.log(err));
}

const del = async (st_del) => {
  return await models.students.destroy(st_del)
  .then(async (data) => { return { students: await assemberData(data.dataValues)}})
  .catch(err => console.log(err));
}

const update = async (new_stud) => {
  return await models.students.findOne({where: {id : new_stud.id}})
  .then(async(stud) => {
    if(stud){
      stud = await stud.update(new_stud);
      return { students: await assemberData(stud.dataValues) };
    }
    return {students : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const getGroup = async(id) => {
 return await groupControll.findOne(id);
}

const assemberData = async (dataValues) => {
  let data = { ...dataValues };
  data.group = await getGroup(data.groupsId) ? await getGroup(data.groupsId) : data.groupsId;
  delete data.groupsId;
  return data;
}

module.exports = {
  findAll,
  create,
  del,
  update,
  findOne,
  assemberData
}