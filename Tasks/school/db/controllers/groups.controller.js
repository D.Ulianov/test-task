const { models } = require('..');

const findAll = async () => {
  return await models.groups.findAll()
  .then((data) => {
    if(data) return { groups: data };
    return {groups : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const findOne = async (id) => {
  return await models.groups.findOne({where: {id}})
  .then((data) => {
    if(data) return { groups: data };
    return {groups : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const findByName = async (name) => {
  return await models.groups.findOne({where: {name}})
  .then((data) => {
    if(data) return { groups: data };
    return {groups : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const del = async (gr_del) => {
  return await models.groups.destroy(gr_del)
  .then((data) => { return { groups: data}})
  .catch(err => console.log(err));
}

module.exports = {
  findAll,
  del,
  findOne,
  findByName
}