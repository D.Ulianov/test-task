const { models } = require('..');

const findAll = async () => {
  return await models.lessons.findAll()
  .then((less) => {
    if(less) return { lessons: less };
    return {lessons : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const findOne = async (id) => {
  return await models.lessons.findOne({where: {id}})
  .then((less) => {
    if(less) return { lessons: less };
    return {lessons : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

const create = async (new_les) => {
 return await models.lessons.create(new_les)
  .then((data) => { return { lessons: data}})
  .catch(err => console.log(err));
}

const del = async (les_del) => {
  return await models.lessons.destroy(les_del)
  .then((data) => { return { lessons: data}})
  .catch(err => console.log(err));
}

const update = async (new_less) => {
  return await models.lessons.findOne({where: {id : new_less.id}})
  .then(async(less) => {
    if(less){
      less = await less.update(new_less);
      return { lessons: less };
    }
    return {lessons : undefined};
  }).catch(err => { 
    console.log(err);
    return { err };
  })
}

module.exports = {
  findAll,
  create,
  del,
  update,
  findOne
}