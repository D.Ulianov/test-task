const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("Lessons", "me", "12345678", 
{
  dialect: "mysql",
  host: "localhost",
  define: {
    timestamps: false
  }
});

require('./models/lessons.model')(sequelize);
require('./models/groups.model')(sequelize);
require('./models/students.model')(sequelize);
require('./models').applySetupModel(sequelize);

sequelize.sync().then(() => {
  console.log('Connected to DB...');
}).catch(err => console.log(err));

module.exports = sequelize;