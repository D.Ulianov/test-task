const { models } = require('../../db');
const lessonDb = require('../../db/controllers/lessons.controller');

const lesson = async function(req, res){
  let result = await lessonDb.findAll();
  if( result.err || !result.lessons) return res.send("error don`t find lessons") 
  res.render("index.hbs", {
    lessons: result.lessons
  })
}

const newLessonGet = async function(req, res) {
  await res.render('create.lesson.hbs')
}

const newLessonPost = async function(req, res) {
   let result = await lessonDb.create({
    name: req.body.name, 
    topic: req.body.topic, 
    group: req.body.group, 
    teacher: req.body.teacher,
    room: req.body.room,
    time: req.body.time});
   if(!result.lessons || result.err ) return res.send("error don`t find lessons");
   res.redirect("/lessons")
}

const lessonDelete = async function (req, res) {
  let result = await lessonDb.del( {where: {id: req.params.id}} )
  if(!result.lessons || result.err ) return res.send("error don`t find lessons");
   res.redirect("/lessons");
}

const lessonUpdateGet = async function (req, res) {
  let result = await lessonDb.findOne(req.params.id)
  if(!result.lessons || result.err ) return res.send("error don`t find lessons");
    res.render("edit.lesson.hbs", {
      lessons: result.lessons
    });
  
}

const lessonUpdatePost = async function (req, res) {
  let result = await lessonDb.update({
    id: req.body.id,
    name: req.body.name, 
    topic: req.body.topic, 
    group: req.body.group, 
    teacher: req.body.teacher,
    room: req.body.room,
    time: req.body.time});
   if(!result.lessons || result.err ) return res.send("error don`t find lessons");
   res.redirect("/lessons")
}

module.exports = {
    lesson,
    newLessonGet,
    newLessonPost,
    lessonDelete,
    lessonUpdateGet,
    lessonUpdatePost
}