const { models } = require('../../db');
const groupDb = require('../../db/controllers/groups.controller');

const group = async function(req, res){
  let result = await groupDb.findAll();
  if( result.err || !result.groups) return res.send("error don`t find groups") 
  res.render("groups.hbs", {
    groups: result.groups
  })
}

const groupDelete = async function (req, res) {
  let result = await groupDb.del( {where: {id: req.params.id}} )
  if(!result.groups || result.err ) return res.send("error don`t find groups");
   res.redirect("/groups");
}

module.exports = {
    group,
    groupDelete
}