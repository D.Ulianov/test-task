const { models } = require('../../db');
const studentDb = require('../../db/controllers/students.controller');
const groupsDb = require('../../db/controllers/groups.controller');


const student = async function(req, res){
  let result = await studentDb.findAll();
  if( result.err || !result.students) return res.send("error don`t find students main") 
  res.render("students.hbs", {
    students: result.students
  })
}

const newStudentGet = async function(req, res) {
  await res.render('create.student.hbs')
}

const newStudentPost = async function(req, res) {
    let { groups } = await groupsDb.findByName(req.body.group);
   let result = await studentDb.create({
    name: req.body.name, 
    age: req.body.age,
    groupId: groups.id});
   if(!result.students || result.err ) return res.send("error don`t find students post");
   res.redirect("/students")
}

const studentDelete = async function (req, res) {
  let result = await studentDb.del( {where: {id: req.params.id}} )
  if(!result.students || result.err ) return res.send("error don`t find students delete");
   res.redirect("/students");
}

const studentUpdateGet = async function (req, res) {
  let result = await studentDb.findOne(req.params.id)
  if(!result.students || result.err ) return res.send("error don`t find students update get");
    res.render("edit.student.hbs", {
      students: result.students
    });
  
}

const studentUpdatePost = async function (req, res) {
  let result = await studentDb.update({
    id: req.body.id,
    name: req.body.name, 
    age: req.body.age});
   if(!result.students || result.err ) return res.send("error don`t find students update post");
   res.redirect("/students")
}

module.exports = {
    student,
    newStudentGet,
    newStudentPost,
    studentDelete,
    studentUpdateGet,
    studentUpdatePost
}