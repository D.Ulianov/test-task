const { Router } = require('express');
const frontRoute = new Router();
const lessRoute = require('./lessons');
const groupRoute = require('./groups');
const studRoute = require('./students');

frontRoute.use('/lessons', lessRoute);
frontRoute.use('/groups', groupRoute);
frontRoute.use('/students', studRoute);


module.exports = frontRoute;