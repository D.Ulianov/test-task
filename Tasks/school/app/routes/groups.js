const express = require('express');
const groupRouter = express.Router();
const Group = require("../controlls/groupControl")

groupRouter.route('/').get(Group.group);
groupRouter.route('/:id/delete').post(Group.groupDelete);


module.exports = groupRouter;