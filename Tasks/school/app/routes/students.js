const express = require('express');
const studentRouter = express.Router();
const Student = require("../controlls/studentControl")

studentRouter.route('/').get(Student.student);

studentRouter.route('/create').get(Student.newStudentGet);
studentRouter.route('/create').post(Student.newStudentPost);
studentRouter.route('/:id/delete').post(Student.studentDelete);
studentRouter.route('/:id/update').get(Student.studentUpdateGet);
studentRouter.route('/:id/update').post(Student.studentUpdatePost);

module.exports = studentRouter;