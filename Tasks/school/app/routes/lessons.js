const express = require('express');
const lessonRouter = express.Router();
const Lesson = require("../controlls/lessonControll")

lessonRouter.route('/').get(Lesson.lesson);

lessonRouter.route('/create').get(Lesson.newLessonGet);
lessonRouter.route('/create').post(Lesson.newLessonPost);
lessonRouter.route('/:id/delete').post(Lesson.lessonDelete);
lessonRouter.route('/:id/update').get(Lesson.lessonUpdateGet);
lessonRouter.route('/:id/update').post(Lesson.lessonUpdatePost);

module.exports = lessonRouter;