const express = require("express");
const app = express();
const route = require('./routes')
const path = require('path')
const db = require("../db");
const bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', route);

app.use(express.static(path.join(__dirname, './public')));
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, './public/views'))
app.listen(3000, function() {
  console.log("Сервер ожидает подключения...");
});