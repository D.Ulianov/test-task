let testFunc = function (obj) {
    let output = [];

    Object.getOwnPropertyNames(obj).forEach(function(val) {
        let key = {}
        key[val] = obj[val].reduce((x, y) => x + y.value, 0);
        output.push(key); 
        output = output.sort((a, b) => Object.values(b)[0] - Object.values(a)[0]);
      });   
      console.log(output);
}

module.export = {
    testFunc
}
