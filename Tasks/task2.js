let blocks = [
  {width: 10, height: 20}, 
  {width: 30, height: 10}, 
  {width: 5, height: 30},
  {width: 15, height: 20},
  {width: 10, height: 5},
{width: 20, height: 5},
{width: 30, height: 5}
];

GrowingPacker = function() {};

GrowingPacker.prototype = {

fit: function(blocks) {
  let n, node, block, len = blocks.length;
  let w = len > 0 ? blocks[0].width : 0;
  let h = len > 0 ? blocks[0].height : 0;
  this.root = { x: 0, y: 0, w: w, h: h };
  for (n = 0; n < len; n++) {
    block = blocks[n];
    if (node = this.findNode(this.root, block.width, block.height))
      block.fit = this.splitNode(node, block.width, block.height);
    else
      block.fit = this.growNode(block.width, block.height);
  }
},

findNode: function(root, w, h) {
  if (root.used)
    return this.findNode(root.right, w, h) || this.findNode(root.down, w, h);
  else if ((w <= root.w) && (h <= root.h))
    return root;
  else
    return null;
},

splitNode: function(node, w, h) {
  node.used = true;
  node.down  = { x: node.x,     y: node.y + h, w: node.w,     h: node.h - h };
  node.right = { x: node.x + w, y: node.y,     w: node.w - w, h: h          };
  return node;
},

growNode: function(w, h) {
  let canGrowDown  = (w <= this.root.w);
  let canGrowRight = (h <= this.root.h);

  let shouldGrowRight = canGrowRight && (this.root.h >= (this.root.w + w)); 
  let shouldGrowDown  = canGrowDown  && (this.root.w >= (this.root.h + h)); 

  if (shouldGrowRight)
    return this.growRight(w, h);
  else if (shouldGrowDown)
    return this.growDown(w, h);
  else if (canGrowRight)
   return this.growRight(w, h);
  else if (canGrowDown)
    return this.growDown(w, h);
  else
    return null;
},

growRight: function(w, h) {
  this.root = {
    used: true,
    x: 0,
    y: 0,
    w: this.root.w + w,
    h: this.root.h,
    down: this.root,
    right: { x: this.root.w, y: 0, w: w, h: this.root.h }
  };
  if (node = this.findNode(this.root, w, h))
    return this.splitNode(node, w, h);
  else
    return null;
},

growDown: function(w, h) {
  this.root = {
    used: true,
    x: 0,
    y: 0,
    w: this.root.w,
    h: this.root.h + h,
    down:  { x: 0, y: this.root.h, w: this.root.w, h: h },
    right: this.root
  };
  if (node = this.findNode(this.root, w, h))
    return this.splitNode(node, w, h);
  else
    return null;
}

}

let packer = new GrowingPacker();
packer.fit(blocks);

let area = {
area_width: packer.root.w,
area_height: packer.root.h,
rectangles: []
};

for(let n = 0; n < blocks.length; n++) {

  let block = {
  position: {x: blocks[n].fit.x, y: blocks[n].fit.y},
  size: {width: blocks[n].width, height: blocks[n].height}
}

area.rectangles.push(block);	
}
console.log(`area_width: ${area.area_width}, \narea_height: ${area.area_height}, \nrectangles: \n`, area.rectangles);
